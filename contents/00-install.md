## 1. 종속성 설치
### 1) python build 종속성 설치
```bash
$ sudo yum install -y python-devel libffi-devel gcc openssl-devel libselinux-python
```
### 2) 가상 환경을 사용하는 종속성 설치
```bash
$ sudo yum install -y python-virtualenv
```
```bash
$ virtualenv ~/kolla
```
```bash
$ source ~/kolla/bin/activate
```
```bash
$ pip install -U pip
```
```bash
$ pip install 'ansible==2.9'
```

## 2. kolla-ansible 설치
### 1) kolla-ansible 파일 다운로드
```bash
$ pip install kolla-ansible
```
### 2) /etc/kolla 디렉토리 생성
```bash
$ sudo mkdir -p /etc/kolla
```
```bash
$ sudo chown $USER:$USER /etc/kolla
```

### 3) globals.yml, password.yml 파일 복사
```bash
$ cp -r ~/kolla/share/kolla-ansible/etc_examples/kolla/* /etc/kolla
```

### 4) 인벤토리 파일 복사
```bash
$ cp ~/kolla/share/kolla-ansible/ansible/inventory/* .
```

## 3. ansible 구성
### 1) 설정파일 수정
```bash
$ sudo mkdir /etc/ansible
```
> $ sudo vi /etc/ansible/ansible.cfg
```
[defaults]
host_key_checking=False
pipelining=True
forks=100
```

### 2) 키기반 설정
```
$ ssh-keygen
$ ssh-copy-id $USER@localhost
$ ssh-copy-id $USER@192.168.122.11
```

### 3) 인벤토리 수정
> $ vi multinode
```
[all:vars]
ansible_become_pass='dkagh1.'

[control]
control01       ansible_host=192.168.122.11  ansible_become=true

[network]
network01       ansible_host=192.168.122.11  ansible_become=true

[compute]
compute01      ansible_host=192.168.122.21  ansible_become=true

[monitoring]
monitoring01    ansible_host=192.168.122.11  ansible_become=true

[storage]
storage01       ansible_host=192.168.122.11  ansible_become=true

[deployment]
localhost       ansible_host=192.168.122.11  ansible_become=true
...
```

### 4) 패스워드 파일 수정
```bash
$ kolla-genpwd
```
> $ vi /etc/kolla/passwords.yml
```yaml
...
keystone_admin_password: dkagh1.
...
```

### 5) 변수 파일 조정
> $ vi /etc/kolla/globals.yml
```yaml
 15 kolla_base_distro: "centos"
 18 kolla_install_type: "binary"
 21 openstack_release: "train"
 37 kolla_internal_vip_address: "192.168.123.250"
 48 kolla_external_vip_address: "192.168.122.250"
 98 network_interface: "eth1"
102 kolla_external_vip_interface: "eth0"
130 neutron_external_interface: "eth2"
248 enable_cinder: "yes"
252 enable_cinder_backend_lvm: "yes"
434 glance_backend_file: "yes"
```

## 4. 스토리지 준비
### cinder-volumes 준비
```bash
$ sudo pvcreate /dev/vdb
$ sudo vgcreate cinder-volumes /dev/vdb 
```

## 5. 배포
### 1) bootstrap
```bash
$ kolla-ansible -i ./multinode bootstrap-servers
```

### 2) prechecks
```bash
$ kolla-ansible -i ./multinode prechecks
```

### 3) image pull
```bash
$ kolla-ansible -i ./multinode pull
```

### 4) deploy
```bash
$ kolla-ansible -i ./multinode deploy
```
